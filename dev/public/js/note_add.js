$(document).ready(function() {
    let $text = "";
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })

    $('.note-box').on('click', function ($text) {
        //$(this).prop('readonly', null);
        $text = $(this).val();
        return $text;
    });

    $('.note-box').on('focusout', function (event) {

        //only send ajax is content changed
        if($(this).val() != $text) {
            $link = $(this).parent().find('a').attr('href');
            $.ajax({
                url: $link + '/update/note',
                method: 'PUT',
                data: $(this).val()
            });
        }
    });

});