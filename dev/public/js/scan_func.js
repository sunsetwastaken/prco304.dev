$(document).ready(function() {
    $("#scanSelect").val(0);

    //hide the awful symfony forms
    /*$( "#command_target_0" ).toggle(function() {
        console.log("hiding sym");
    });*/


    /*let $type = "";
    let $config = "";
    let $target = "";*/

    // clicking on Vuln Scan
    $( "#SelectScan" ).on('click', function(){

        // if the other option is selected, de-select it
        if($("#SelectHost").hasClass('selected-type')){
            $( "#SelectHost" ).trigger( "click" );
        }



        $("#scan_command_module").val("scan");
        $("#scan_command_options_0").val("");
        $("#SelectScanNormal").removeClass('selected-type');
        $("#SelectScanFast").removeClass('selected-type');
        $("#SelectScanAll").removeClass('selected-type');
        $("#SelectHostNormal").removeClass('selected-type');
        $("#SelectHostFast").removeClass('selected-type');
        $("#scan_command_target_0").val("");
        $("#hostAll").removeClass('selected-type');
        $("#hostRange").removeClass('selected-type');
        $("#scanSelect").val(0);

        $(this).toggleClass('selected-type');

        $( "#SelectScanConf" ).toggle(function() {
            console.log("toggling start");
        });
        $( "#SelectScanTarget" ).toggle(function() {
            console.log("toggling start");
        });
        $( "#submitBtn" ).toggle(function() {
            console.log("toggling start");
        });
    });

    // Clicking on Host Discovery
    $( "#SelectHost" ).on('click', function(){
        // if the other option is selected, de-select it
        if($("#SelectScan").hasClass('selected-type')){
            $( "#SelectScan" ).trigger( "click" );
        }

        $("#scan_command_module").val("disc");
        $("#scan_command_options_0").val("");
        $("#SelectScanNormal").removeClass('selected-type');
        $("#SelectScanFast").removeClass('selected-type');
        $("#SelectScanAll").removeClass('selected-type');
        $("#SelectHostNormal").removeClass('selected-type');
        $("#SelectHostFast").removeClass('selected-type');
        $("#scan_command_target_0").val("");
        $("#hostAll").removeClass('selected-type');
        $("#hostRange").removeClass('selected-type');
        $("#scanSelect").val(0);


        $(this).toggleClass('selected-type');

        $( "#SelectHostConf" ).toggle(function() {
            console.log("toggling start");
        });
        $( "#SelectHostTarget" ).toggle(function() {
            console.log("toggling start");
        });
        $( "#submitBtn" ).toggle(function() {
            console.log("toggling start");
        });
    });

    // click on a SCAN config
    $( "#SelectScanNormal" ).on('click', function() {
        $("#scan_command_options_0").val(1); // converted to "-sC -sV"
        $(this).toggleClass('selected-type');

        //disable clicked effect for others (in future don't have this hard coded)
        $("#SelectScanFast").removeClass('selected-type');
        $("#SelectScanAll").removeClass('selected-type');
        $("#SelectHostNormal").removeClass('selected-type');
        $("#SelectHostFast").removeClass('selected-type');

    });

    $( "#SelectScanFast" ).on('click', function() {
        $("#scan_command_options_0").val(2); // converted to "-sC -sV -T5"
        $(this).toggleClass('selected-type');

        $("#SelectScanNormal").removeClass('selected-type');
        $("#SelectScanAll").removeClass('selected-type');
        $("#SelectHostNormal").removeClass('selected-type');
        $("#SelectHostFast").removeClass('selected-type');
    });

    $( "#SelectScanAll" ).on('click', function() {
        $("#scan_command_options_0").val(3); // converted to -sC -sV -p-
        $(this).toggleClass('selected-type');

        $("#SelectScanFast").removeClass('selected-type');
        $("#SelectScanNormal").removeClass('selected-type');
        $("#SelectHostNormal").removeClass('selected-type');
        $("#SelectHostFast").removeClass('selected-type');
    });

    // click on a HOST config
    $( "#SelectHostNormal" ).on('click', function() {
        $("#scan_command_options_0").val(1); // converted to -sn -PE
        $(this).toggleClass('selected-type');

        $("#SelectScanFast").removeClass('selected-type');
        $("#SelectScanAll").removeClass('selected-type');
        $("#SelectScanNormal").removeClass('selected-type');
        $("#SelectHostFast").removeClass('selected-type');
    });

    $( "#SelectHostFast" ).on('click', function() {
        $("#scan_command_options_0").val(2); // converted to -sn -PE -T5
        $(this).toggleClass('selected-type');

        $("#SelectScanFast").removeClass('selected-type');
        $("#SelectScanAll").removeClass('selected-type');
        $("#SelectHostNormal").removeClass('selected-type');
        $("#SelectScanNormal").removeClass('selected-type');
    });

    // Clicking HOST All targets
    $( "#hostAll" ).on('click', function() {
        $("#scan_command_target_0").val("10.10.10.1/24");
        $(this).toggleClass('selected-type');

        $("#hostRange").removeClass('selected-type');
        $("#scanSelect").val(0);
    });

    // Clicking custom HOST target Range
    $( "#hostRange" ).on('click', function() {
        $(this).toggleClass('selected-type');

        $("#hostAll").removeClass('selected-type');
        $("#scanSelect").val(0);
    });

    // CLICKING SUBMIT
    $("#submitBtn" ).on('click', function(event) {
        //set symfony forms to be valid
        if($( "#hostRange" ).hasClass('selected-type')){
            $("#scan_command_target_0").val(
                $( "#hostRangeInput" ).val()
            )
        }

        if($("#scan_command_module").val() == ""){
            alert("Please select a Module");
        }
        else if($("#scan_command_options_0").val() == ""){
            alert("Please select a scan Configuration");
        }
        else if($("#scan_command_target_0").val() == ""){
            alert("Please ensure you have selected a target");
        }

        //then check if everything is selected before submitting
        //alert($target);
        //$("#command_target_0").val($target);

    });

    //changing selected host
    $('#scanSelect').on('change', function ($target) {
        $target = $(this).children(':selected').attr('id');
        $("#scan_command_target_0").val($target);

        $("#hostRange").removeClass('selected-type');
        $("#hostAll").removeClass('selected-type');
        return $target;
    });

    // so everything is displayed on page load (better HCI)
    $( "#SelectHost" ).trigger('click');
});