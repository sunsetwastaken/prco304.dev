$(document).ready(function() {

    $('#scanSelect').on('change', function (e) {
        $md5 = $(this).children(':selected').attr('id');


        window.location.href = "/scan/"+$md5;
    });

    $('#newScanBox').on('click', function (e) {
        $('#submitScanBtn').trigger('click');
    });

    $('#closePendingInfo').on('click', function (e) {
        $('#pendingInfoBox').toggle();
        $('#pendingInfoSpacer').toggle();
    });
});