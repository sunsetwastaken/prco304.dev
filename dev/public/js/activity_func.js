$(document).ready(function() {

    $('.confirmCancel').on('click', function () {
        $.ajax({
            url: '/activity/'+ $(this).attr('id') +'/update/cancel',
            method: 'PUT',
        });
        // give put request 100ms to fire then reload the page
        setTimeout(location.reload.bind(location), 50);
    });
});