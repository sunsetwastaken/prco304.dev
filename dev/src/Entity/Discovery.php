<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DiscoveryRepository")
 */
class Discovery extends Command
{
    /*/**
     * @ORM\OneToMany(targetEntity="App\Entity\Host", mappedBy="lastSeenBy")
     *//*
    private $hosts;

    public function __construct(User $user, string $module, array $options, array $target)
    {
        parent::__construct($user, $module, $options, $target);
        $this->hosts = new ArrayCollection();
    }*/

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DiscoveryData", mappedBy="discovery")
     */
    private $data;

    public function __construct(User $user, string $module, array $options, array $target)
    {
        parent::__construct($user, $module, $options, $target);
        $this->data = new ArrayCollection();
    }

    /**
     * @return Collection|DiscoveryData[]
     */
    public function getData(): Collection
    {
        return $this->data;
    }
}
