<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DiscoveryDataRepository")
 */
class DiscoveryData
{
    /**
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="App\Entity\Discovery", inversedBy="data")
     * @ORM\JoinColumn(nullable=false)
     */
    private $discovery;

    /**
     * @ORM\Id()
     * @ORM\OneToOne(targetEntity="host")
     * @ORM\JoinColumn(nullable=false)
     */
    private $host;

    public function __construct(Discovery $discovery, Host $host)
    {
        $this->host = $host;
        $this->discovery = $discovery;
    }

    /**
     * @return Host
     */
    public function getHost(): Host
    {
        return $this->host;
    }
}
