<?php

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CommandRepository")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({"command" = "Command", "discovery" = "Discovery"})
 */

class Command
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="commands")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $module;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $options = [];

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $target = [];

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $creationTime;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $OTP;

    /**
     * @ORM\Column(type="string", length=180, nullable=true)
     */
    private $completionTime;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Scan", mappedBy="command")
     */
    private $scans;

    // Command can almost be immutable - other than complete status
    // immutable entities don't work well with forms, re-introduced setters for module, options, and target
    public function __construct(User $user, string $module, array $options, array $target){

        $random = random_bytes(32);

        // md5 is suitable for mapping uploaded content, not used for crypto,
        // any string is effectively okay here "Elizabeth-command-2" would also be fine
        // the weaknesses of md5 aren't relevant here
        $OTP = hash('md5',$random);

        $this->user = $user;
        $this->module = $module;
        $this->options = $options;
        $this->target = $target;
        $this->creationTime = time();
        $this->OTP = $OTP;
        $this->complete = false;

    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function getModule(): ?string
    {
        return $this->module;
    }

    // necessary for form usage
    public function setModule(string $module): self
    {
        $this->module = $module;
        return $this;
    }

    public function getOptions(): ?array
    {
        return $this->options;
    }

    // necessary for form usage
    public function setOptions(?array $options): self
    {
        $this->options = $options;
        return $this;
    }

    public function getTarget(): ?array
    {
        return $this->target;
    }

    // necessary for form usage
    public function setTarget(?array $target): self
    {
        $this->target = $target;
        return $this;
    }

    public function getCreationTime(): ?string
    {
        return $this->creationTime;
    }


    public function getOTP(): ?string
    {
        return $this->OTP;
    }

    public function getCompletionTime(): ?string
    {
        return $this->completionTime;
    }

    public function setCompletionTime(?string $completionTime): self
    {
        $this->completionTime = $completionTime;

        return $this;
    }

    /*public function setComplete(bool $complete): self
    {
        $this->complete = $complete;

        return $this;
    }*/

    /**
     * @return Collection|Scan[]
     */
    public function getScans(): Collection
    {
        return $this->scans;
    }

    /*
    public function addScan(Scan $scan): self
    {
        if (!$this->scans->contains($scan)) {
            $this->scans[] = $scan;
            $scan->setHost($this);
        }

        return $this;
    }*/

    public function removeScan(Scan $scan): self
    {
        if ($this->scans->contains($scan)) {
            $this->scans->removeElement($scan);
            // set the owning side to null (unless already changed)
            if ($scan->getHost() === $this) {
                $scan->setHost(null);
            }
        }

        return $this;
    }
}
