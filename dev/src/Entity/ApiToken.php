<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ApiTokenRepository")
 */
class ApiToken
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $token;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="apiTokens")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $expiry;

    public function __construct(User $user)
    {
        //random_bytes aims to be entirely random
        $random = random_bytes(32);
        //uniqid makes the key unique, lowering randomness slightly, but also the chance of collisions.
        $unique = uniqid("",true);
        $key = hash('sha256',$random . $unique);

        $this->token = $key;
        $this->user = $user;
        $this->expiry = strtotime("+2 weeks");
    }

    /* Most values can't change from initial values (set on instantiation by constructor),
        If we wanted to update token, we generate a new one etc.

        Todo: For Auto-renew functionality, $this->expiry may update*/

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function getExpiry(): ?string
    {
        return $this->expiry;
    }

    public function renewExpiry()
    {
        // sets expiry to 2 weeks from now
        $this->expiry = strtotime("+2 weeks");
    }
}
