<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CVERepository")
 */
class CVE
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Port", inversedBy="CVEs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $port;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $name;

    /**
     * @ORM\Column(type="float")
     */
    private $cvss;

    /**
     * @ORM\Column(type="text")
     */
    private $summary;

    public function __construct($port, $name, $cvss, $summary)
    {
        $this->port = $port;
        $this->name = $name;
        $this->cvss = $cvss;
        $this->summary = $summary;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPort(): ?Port
    {
        return $this->port;
    }

    public function setPort(?Port $port): self
    {
        $this->port = $port;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCvss(): ?float
    {
        return $this->cvss;
    }

    public function setCvss(float $cvss): self
    {
        $this->cvss = $cvss;

        return $this;
    }

    public function getSummary(): ?string
    {
        return $this->summary;
    }

    public function setSummary(string $summary): self
    {
        $this->summary = $summary;

        return $this;
    }
}
