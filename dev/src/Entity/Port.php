<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PortRepository")
 */
class Port
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Scan", inversedBy="ports")
     * @ORM\JoinColumn(nullable=false)
     */
    private $scan;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $number;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $protocol;

    /**
     * @ORM\Column(type="string", length=180, nullable=true)
     */
    private $service;

    /**
     * @ORM\Column(type="string", length=180, nullable=true)
     */
    private $version;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=180, nullable=true)
     */
    private $cpe;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CVE", mappedBy="port", orphanRemoval=true)
     */
    private $CVEs;

    public function __construct(Scan $scan, string $number, string $protocol, ?string $service, ?string $version, ?string $status, ?string $cpe)
    {
        $this->scan = $scan;
        $this->number = $number;
        $this->protocol = $protocol;
        $this->service = $service;
        $this->version = $version;
        $this->status = $status;
        $this->cpe = $cpe;
        $this->CVEs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getScan(): ?Scan
    {
        return $this->scan;
    }

    public function setScan(?Scan $scan): self
    {
        $this->scan = $scan;

        return $this;
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function setNumber(string $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getProtocol(): ?string
    {
        return $this->protocol;
    }

    public function setProtocol(string $protocol): self
    {
        $this->protocol = $protocol;

        return $this;
    }

    public function getService(): ?string
    {
        return $this->service;
    }

    public function setService(?string $service): self
    {
        $this->service = $service;

        return $this;
    }

    public function getVersion(): ?string
    {
        return $this->version;
    }

    public function setVersion(?string $version): self
    {
        $this->version = $version;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCpe(): ?string
    {
        return $this->cpe;
    }

    public function setCpe(?string $cpe): self
    {
        $this->cpe = $cpe;

        return $this;
    }

    /**
     * @return Collection|CVE[]
     */
    public function getCVEs(): Collection
    {
        return $this->CVEs;
    }

    public function addCVE(CVE $cVE): self
    {
        if (!$this->CVEs->contains($cVE)) {
            $this->CVEs[] = $cVE;
            $cVE->setPort($this);
        }

        return $this;
    }

    public function removeCVE(CVE $cVE): self
    {
        if ($this->CVEs->contains($cVE)) {
            $this->CVEs->removeElement($cVE);
            // set the owning side to null (unless already changed)
            if ($cVE->getPort() === $this) {
                $cVE->setPort(null);
            }
        }

        return $this;
    }
}
