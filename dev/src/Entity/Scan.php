<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ScanRepository")
 */
class Scan
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
    // necessary for port objects to refer to

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Command", inversedBy="scans")
     * @ORM\JoinColumn(nullable=false)
     */
    private $command;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Host", inversedBy="scans")
     * @ORM\JoinColumn(nullable=false)
     */
    private $host;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Port", mappedBy="scan", orphanRemoval=true)
     */
    private $ports;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $md5;

    // effectively immutable (as a db entry, ports relation will change)
    public function __construct(Command $command, Host $host)
    {
        $random = random_bytes(16);

        $this->command = $command;
        $this->host = $host;
        $this->md5 = hash('md5',$random);
        $this->ports = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCommand(): Command
    {
        return $this->command;
    }

    public function getHost(): Host
    {
        return $this->host;
    }

    /**
     * @return Collection|Port[]
     */
    public function getPorts(): Collection
    {
        return $this->ports;
    }

    public function addPort(Port $port): self
    {
        if (!$this->ports->contains($port)) {
            $this->ports[] = $port;
            $port->setScan($this);
        }

        return $this;
    }

    public function removePort(Port $port): self
    {
        if ($this->ports->contains($port)) {
            $this->ports->removeElement($port);
            // set the owning side to null (unless already changed)
            if ($port->getScan() === $this) {
                $port->setScan(null);
            }
        }

        return $this;
    }

    public function getMd5(): ?string
    {
        return $this->md5;
    }
}
