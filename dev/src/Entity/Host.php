<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HostRepository")
 */
class Host
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="hosts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=180, nullable=true)
     */
    private $detail;

    /**
     * @ORM\Column(type="string", length=180, nullable=true)
     */
    private $note;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $address;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Command")
     */
    private $discovery;

    /**
     * @ORM\Column(type="string", length=180, nullable=true)
     */
    private $hostname;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Command")
     */
    private $lastSeenBy;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Scan", mappedBy="host", orphanRemoval=true)
     */
    private $scans;

    /**
     * @ORM\Column(type="string", length=180)
     */
    // it's computable, but being one-way it's required for lookups
    private $md5;

    public function __construct(User $user, string $address, Command $discovery, Command $lastSeenBy)
    {
        $random = random_bytes(16);

        $this->user = $user;
        $this->address = $address;
        $this->discovery = $discovery;
        $this->lastSeenBy = $lastSeenBy;
        $this->md5 = hash('md5',$random);
        $this->scans = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    // todo: wouldn't be needed outside constructor
    // serializer uses these though
    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getDetail(): ?string
    {
        return $this->detail;
    }

    public function setDetail(?string $detail): self
    {
        $this->detail = $detail;

        return $this;
    }

    public function getNote(): ?string
    {
        return $this->note;
    }

    public function setNote(?string $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

 /* Neither are required now, since use of constructor ensures address is already present
 // todo: having these two seperate getters is inelegant but functional
    public function setAddress(array $xmlstuff): self
    {
        // accessing xml attributes during deserialization doesn't work
        // Literally spent like all day trying to.
        // this is a harmless work around.

        //dd($xmlstuff);
        $this->address = $xmlstuff['@addr'];
        return $this;
    }

    // todo - replace as setAddress
    public function setAddressString(string $address): self
    {
        $this->address = $address;

        return $this;
    }*/

    public function getDiscovery(): ?Command
    {
        return $this->discovery;
    }

    public function setDiscovery(Command $discovery): self
    {
        $this->discovery = $discovery;

        return $this;
    }

    public function getHostname(): ?string
    {
        return $this->hostname;
    }

    public function setHostname(?string $hostname): self
    {
        $this->hostname = $hostname;

        return $this;
    }

    public function getLastSeenBy(): ?Command
    {
        return $this->lastSeenBy;
    }

    public function setLastSeenBy(?Command $lastSeenBy): self
    {
        $this->lastSeenBy = $lastSeenBy;

        return $this;
    }


    /**
     * @return Collection|Scan[]
     */
    public function getScans(): Collection
    {
        return $this->scans;
    }

    public function getScanByMd5(string $string): ?Scan
    {
        foreach($this->scans as $scan){
            if($scan->getMd5() == $string){
                return $scan;
            }
        }
    }

    public function getLastScan(): ?Scan
    {
        return $this->scans->last();
    }

    /* not needed, scans are constructed independently
    public function addScan(Scan $scan, Command $command): self
    {
        if (!$this->scans->contains($scan)) {
            $this->scans[] = $scan;
            $scan->setHost($this);
        }

        return $this;
    }*/

    public function removeScan(Scan $scan): self
    {
        if ($this->scans->contains($scan)) {
            $this->scans->removeElement($scan);
            // set the owning side to null (unless already changed)
            if ($scan->getHost() === $this) {
                $scan->setHost(null);
            }
        }

        return $this;
    }

    public function getMd5(): ?string
    {
        return $this->md5;
    }
}
