<?php

namespace App\Entity;

use App\Repository\DiscoveryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity(fields={"username"}, message="This account already exists")
 * @UniqueEntity(fields={"email"}, message="This account already exists")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $username;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=180, nullable=true, unique=true)
     * @Assert\Email(message = "The email '{{ value }}' is not a valid email.")
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $creationTime;

    /**
     * @ORM\Column(type="string", length=180, nullable=true)
     */
    private $lastLoginTime;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Report", mappedBy="user", orphanRemoval=true)
     */
    private $reports;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Host", mappedBy="user", orphanRemoval=true)
     */
    private $hosts;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Command", mappedBy="user", orphanRemoval=true)
     */
    private $commands;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ApiToken", mappedBy="user", orphanRemoval=true)
     */
    private $apiTokens;

    private $discoveries;

    public function __construct()
    {
        $this->reports = new ArrayCollection();
        $this->hosts = new ArrayCollection();
        $this->commands = new ArrayCollection();
        $this->apiTokens = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getCreationTime(): ?string
    {
        return $this->creationTime;
    }

    public function setCreationTime(string $creationTime): self
    {
        $this->creationTime = $creationTime;

        return $this;
    }

    public function getLastLoginTime(): ?string
    {
        return $this->lastLoginTime;
    }

    public function setLastLoginTime(?string $lastLoginTime): self
    {
        $this->lastLoginTime = $lastLoginTime;

        return $this;
    }

    /**
     * @return Collection|Report[]
     */
    public function getReports(): Collection
    {
        return $this->reports;
    }

    public function addReport(Report $report): self
    {
        if (!$this->reports->contains($report)) {
            $this->reports[] = $report;
            $report->setUser($this);
        }

        return $this;
    }

    public function removeReport(Report $report): self
    {
        if ($this->reports->contains($report)) {
            $this->reports->removeElement($report);
            // set the owning side to null (unless already changed)
            if ($report->getUser() === $this) {
                $report->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Host[]
     */
    public function getHosts(): Collection
    {
        return $this->hosts;
    }

    // test function to get specific host entity without re-querying database.
    // I'd need to make generic getter/setter methods instead but that would reduce performance
    /*
    public function getHostBy(string $attribute, string $value): ?Host
    {
        foreach($this->hosts as $host){
            // can't access $host->$attribute because they're all private :|

            if($host->$attribute == $value){
                return $host;
            }
        }
        return null;
    }*/

    public function getHostByAddress(string $value): ?Host
    {
        foreach($this->hosts as $host){
            if($host->getAddress() == $value){
                return $host;
            }
        }
        return null;
    }

    public function getHostByMd5(string $value): ?Host
    {
        foreach($this->hosts as $host){
            if($host->getMd5() == $value){
                return $host;
            }
        }
        return null;
    }


    public function addHost(Host $host): self
    {
        if (!$this->hosts->contains($host)) {
            $this->hosts[] = $host;
            $host->setUser($this);
        }

        return $this;
    }

    public function removeHost(Host $host): self
    {
        if ($this->hosts->contains($host)) {
            $this->hosts->removeElement($host);
            // set the owning side to null (unless already changed)
            if ($host->getUser() === $this) {
                $host->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Command[]
     */
    public function getCommands(): Collection
    {
        return $this->commands;
    }

    // passing these arrays by reference, code efficiency improvement - type goes from : Collection to : void
    public function getCommandCompletionArrays(array &$complete, array &$pending): void
    {
        foreach($this->commands as $command){
            if($command->getCompletionTime()){
                array_push($complete,$command);
            }else{
                array_push($pending,$command);
            }
        }
        //return $this->commands;
    }

    /*public function getPendingCommands(): Collection
    {
        $completedCommands = [];
        $pendingCommands = [];
        return $this->commands;
    }*/

    public function addCommand(Command $command): self
    {
        if (!$this->commands->contains($command)) {
            $this->commands[] = $command;
            $command->setUser($this);
        }

        return $this;
    }

    public function removeCommand(Command $command): self
    {
        if ($this->commands->contains($command)) {
            $this->commands->removeElement($command);
            // set the owning side to null (unless already changed)
            if ($command->getUser() === $this) {
                $command->setUser(null);
            }
        }

        return $this;
    }

    public function getCommandByOTP(string $value): ?Command
    {
        foreach($this->commands as $command){
            if($command->getOTP() == $value){
                return $command;
            }
        }
        return null;
    }



/* todo Testing easier get subclass functionality
    /**
     * @return Collection|Discovery[]
     *//*
    public function getDiscoveries(): Collection
    {
        return $this->getEntityManager()->getRepository('')
            ->findBy(['user' => $this->getUser()]);
    }
*/

    /**
     * @return Collection|ApiToken[]
     */
    public function getApiTokens(): Collection
    {
        return $this->apiTokens;
    }

    public function addApiToken(ApiToken $apiToken): self
    {
        if (!$this->apiTokens->contains($apiToken)) {
            $this->apiTokens[] = $apiToken;
            $apiToken->setUser($this);
        }

        return $this;
    }

    public function removeApiToken(ApiToken $apiToken): self
    {
        if ($this->apiTokens->contains($apiToken)) {
            $this->apiTokens->removeElement($apiToken);
            // set the owning side to null (unless already changed)
            if ($apiToken->getUser() === $this) {
                $apiToken->setUser(null);
            }
        }

        return $this;
    }

    /*/**
     * @return Collection|Discovery[]
     *//*
    public function getDiscoveries(): Collection
    {
        new DiscoveryRepository() =  $discoveryRepository;
        $dRepo = $discoveryRepository->findBy(['user' => $this->getUser()]);
        return $this->commands;
    }*/
}
