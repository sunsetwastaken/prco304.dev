<?php

namespace App\Controller;

use App\Entity\Command;
use App\Entity\DiscoveryData;
use App\Form\CommandType;
use App\Repository\ScanRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ActivityController extends AbstractController
{
    /**
     * @Route("/activity", name="app_activity")
     */
    public function index()
    {
        $completedCommands = [];
        $pendingCommands = [];
        // separate completed commands and pending commands
        $this->getUser()->getCommandCompletionArrays($completedCommands,$pendingCommands);


        /*$discoveryList = [];
        $scanList = [];

        // only get scans and discoveries, others won't have pages
        foreach(array_reverse($completedCommands) as $cmd){
            if(is_a($cmd, "App\Entity\Discovery")){
                array_push($discoveryList, $cmd);
            }
            elseif($scans = $cmd->getScans()){
                foreach ($scans as $scan){
                    array_push($scanList, $scan);
                }
            }
        }

        $commandList = [];

        foreach(array_reverse($completedCommands) as $cmd){
            if(is_a($cmd, "App\Entity\Discovery")){
                array_push($commandList, $cmd);
            }
            elseif($scans = $cmd->getScans()){
                foreach ($scans as $scan){
                    // putting the scan in, NOT the command, to make it different from disc in displaying
                    array_push($commandList, $scan);
                }
            }
        }*/




        // sort completed by weeks
        $weeksCompleted = [];
        $weekCommands = [];

        // read the array in reverse order so the newest commands are at the start of the array
        // benefits twig display
        foreach(array_reverse($completedCommands) as $command) {
            $formattedWeek = date("W-Y", $command->getCompletionTime());
            if (!in_array($formattedWeek, $weeksCompleted)) {
                // check if it has scans; if so add the SCAN entity
                $scans = $command->getScans();
                if($scans[0] != null){
                    for($i=0;$i<count($scans);$i++){
                        if($i == 0){
                            array_push($weekCommands, array($scans[0]));
                            // NEW WEEK, CREATE THEN APPEND
                            array_push($weeksCompleted, $formattedWeek);
                        }
                        else{
                            array_push($weekCommands[count($weekCommands)-1], $scans[$i]);
                        }
                    }
                }
                elseif(is_a($command, "App\Entity\Discovery")){
                    array_push($weekCommands, array($command));
                    // NEW WEEK, CREATE THEN APPEND
                    array_push($weeksCompleted, $formattedWeek);
                }
                else{
                    // if the first scan of the week fails (debugging this was fun >_>)
                    // solution - Weeks are now only created on finding a successful command
                }
            }
            // OLD WEEK, APPEND
            else {
                $scans = $command->getScans();
                if($scans[0] != null){
                    foreach($scans as $scan){
                        array_push($weekCommands[count($weekCommands)-1], $scan);
                    }
                }
                // no scans for this command, is it a disc?
                elseif(is_a($command, "App\Entity\Discovery")){
                    array_push($weekCommands[count($weekCommands)-1], $command);
                }
                /* Failed scans etc would show up here, they aren't discoveries and they didn't produce a report on anything  literally hours wasted ;_;
                else{
                }*/
            }
        }

        $orderedPending = [];
        if($pendingCommands != null) {
            $orderedPending = array_reverse($pendingCommands);
        }


        return $this->render('activity/index.html.twig', [
            'completeCommandsByWeek' => $weekCommands,
            'pendingCommands' => $orderedPending,
        ]);
    }

    /**
     * @Route("/discovery/{slug}", name="command_view")
     */
    public function viewCommand(Request $request, $slug): Response
    {
        // check command has been completed, if not 404
        {
            $command = $this->getUser()->getCommandByOTP($slug);
            if (!$command) {
                throw $this->createNotFoundException('Command does not exist');
            }
            if (!($command->getCompletionTime())) {
                throw $this->createNotFoundException('Command has not yet completed');
            }
            // check command type?
            if(is_a($command, "App\Entity\Discovery")){
                // it's a discovery type call host view
                $hostList = [];
                foreach($command->getData() as $data){
                    array_push($hostList, $data->getHost());
                }

                return $this->render('host/cards.html.twig', [
                    // don't think get data is gonna work here but worth a try
                    // also have get cards to check if there is a command form or not
                    'hosts' =>  $hostList,
                    'commandTime' => $command->getCompletionTime()
                ]);
            }

            throw $this->createNotFoundException('Discovery does not exist');
        }
    }


    /**
     * @Route("/activity/{slug}/update/cancel", name="command_update_cancel", methods={"PUT"})
     */
    public function cancelCommand(Request $request, $slug): Response
    {
        $cmd = $this->getUser()->getCommandByOTP($slug);
        if (!$cmd) {
            throw $this->createNotFoundException('Command does not exist');
        }
        // only remove pending commands, not completed commands
        if(!($cmd->getCompletionTime())){
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($cmd);
            $entityManager->flush();
            return $this->json("Command cancelled successfully", 200);
        }
    }

    /**
     * @Route("/scan/{slug}", name="scan_view")
     */
    public function viewScan(Request $request, $slug, ScanRepository $scanRepository): Response
    {
        $scan = $scanRepository->findOneBy(['md5' => $slug]);
        //$scan = $this->getUser()->getCommandByOTP($slug);

        if($scan){
            $host = $scan->getHost();
            if($host->getUser() == $this->getUser()){
                // make a command form - so the user can issue a scan to the host they're viewing
                $command = new Command(
                    $this->getUser(),
                    "nmap",
                    ["xxx"],
                    ["yyy"]
                );

                $form = $this->createForm(CommandType::class, $command);
                $form->handleRequest($request);

                if ($form->isSubmitted() && $form->isValid()){
                    $entityManager = $this->getDoctrine()->getManager();

                    // decide by button press, not user input
                    $command->setOptions([
                        "-sC","-sV",
                        // output options, saves in /tmp/ as an xml file -> todo %temp% in windows
                        "-oX","/tmp/".$command->getOTP()
                    ]);
                    $command->setTarget([
                        $host->getAddress()
                    ]);

                    $entityManager->persist($command);
                    $entityManager->flush();


                    $userOS = "";
                    $filename = $this->getUser()->getUsername()."_scanner.py";
                    // look for tell-tale string in useragent
                    if (strpos($_SERVER['HTTP_USER_AGENT'], 'Windows')){
                        $userOS =  "Windows";
                    }
                    elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'Linux')){
                        $userOS = "Linux";
                    }

                    if($userOS == ""){
                        return $this->render('host/success.html.twig', [
                            'filename' => $filename,
                        ]);
                    }

                    return $this->render('host/success.html.twig', [
                        'filename' => $filename,
                        'userOS' => $userOS,
                    ]);
                }
                    //get noteworthy services (not application based (DNS,LDAP,etc))
                    $notedPorts = [];
                    foreach($scan->getPorts() as $port){
                        if(!in_array($port->getService(), $notedPorts)){
                            switch($port->getService()){
                                case "domain":
                                    array_push($notedPorts, "domain");
                                    break;
                                case "ldap":
                                    array_push($notedPorts, "ldap");
                                    break;
                                case "telnet?":
                                    array_push($notedPorts, "telnet");
                                    break;
                                case "microsoft-ds":
                                    array_push($notedPorts, "smb");
                                    break;
                            }
                        }
                    }
                    // does this automatically exclude those that are empty - I think so

                    // are there any pending scans for this host?
                $pendingBool = false;

                $pending = [];
                $complete = [];
                $this->getUser()->getCommandCompletionArrays($complete,$pending);

                foreach($pending as $currentPending){
                    if(in_array($host->getAddress(), $currentPending->getTarget())){
                        $pendingBool = true;
                        break;
                    }
                }



                    if(isset($notedPorts)){
                        return $this->render('host/scan.report.html.twig', [
                            'host' => $host,
                            'selectedScan' => $scan,
                            'extraPorts' => $notedPorts,
                            'CommandForm' => $form->createView(),
                            'pendingScan' => $pendingBool,
                        ]);
                    }



                return $this->render('host/scan.report.html.twig', [
                    'host' => $host,
                    'selectedScan' => $scan,
                    'pendingScan' => $pendingBool,
                    'CommandForm' => $form->createView()
                ]);
            }
        }
        throw $this->createNotFoundException('Scan does not exist');
        /*

        if (!$scan) {
            throw $this->createNotFoundException('Command does not exist');
        }
        return $this->render('host/report.html.twig', [

            //'host' => $host,
            //'CommandForm' => $form->createView(),
        ]);*/
    }
}

