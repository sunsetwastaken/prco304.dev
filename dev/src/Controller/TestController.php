<?php

namespace App\Controller;

use App\Entity\Command;
use App\Entity\Scan;
use App\Repository\DiscoveryRepository;
use App\Entity\Discovery;
use MongoDB;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class TestController extends AbstractController
{
    /**
     * @Route("/test", name="app_test")
     */
    public function index(DiscoveryRepository $discoveryRepository)
    {
        /*$test = new Discovery($this->getUser(),"aaa",["bbb"],["ccc"]);
        /*$test->setUser
            /*$this->getUser(),
            "xxx",
            ["xxx"],
            ["10.10.10.1"]
        );*/
        $test = new Discovery(
            $this->getUser(),
            "disc",
            ["test"],
            ["10.10.10.1"]
        );
        $host = $this->getUser()->getHostByAddress('10.10.10.145');
        $test->addHost($host);

        $dRepo = $discoveryRepository->findBy(['user' => $this->getUser()]);


        dd($dRepo);

        dd($this->getUser());
        dd($test);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($test);
        $entityManager->flush();

        $dRepo = $discoveryRepository->findBy(['user' => $this->getUser()]);


        dd($dRepo);


        $hosts = $this->getUser()->getHosts();
        $hostCount = count($hosts);
        return $this->render('account/index.html.twig', [
            'hostCount' => $hostCount,
        ]);
    }
    /**
     * @Route("/testing", name="app_testing")
     */
    public function testing()
    {
        foreach ($this->getUser()->getHosts() as $host){
            $test = new Scan(
                $this->getUser(),
                "nmap",
                ["scan"],
                ["10.10.10.1"],
                $host
            );
            echo("New Scan created<br>
using".$host->getId()." : ".$host->getAddress());
            dd($test);
            break;
        }
        $test->setOptions([
            // scan options
            "-sC","-sV",
            // output options, saves in /tmp/ as an xml file -> todo %temp% in windows
            "-oX","/tmp/".$test->getOTP()
        ]);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($test);
        $entityManager->flush();
        return new JsonResponse(["added"]);
    }

    /**
     * @Route("/testing2", name="app_testing")
     */
    public function testing2()
    {
        $test = new Command($this->getUser(),"nmap",["x"],["10.10.10.145"]);
        $test->setOptions(["-sC", "-sV", "-oX", "/tmp/".$test->getOTP()]);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($test);
        $entityManager->flush();
        dd($test);
        return new JsonResponse(["added"]);
    }

    /**
     * @Route("/testing3", name="app_testing")
     */
    public function testing3(DiscoveryRepository $discoveryRepository)
    {

        //$disc = $this->getUser()->getDiscoveries();
        $dRepo = $discoveryRepository->findBy(['user' => $this->getUser()]);


        dd($dRepo);
        //dd($disc);
        return new JsonResponse(["aaaa"]);
    }

    /**
     * @Route("/testing-disc", name="app_testing")
     */
    public function testingdisc()
    {
        $test = new Discovery($this->getUser(),"nmap",["x"],["10.10.10.140/29"]);
        $test->setOptions(["-sn", "-PE", "-oX", "/tmp/".$test->getOTP()]);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($test);
        $entityManager->flush();
        dd($test);
        return new JsonResponse(["added"]);
    }

    /**
     * @Route("/cpe", name="app_testing3434")
     */
    public function cpetesting()
    {
        $host = $this->getUser()->getHostByAddress("10.10.10.145");
        $scan = $host->getLastScan();
        //echo($scan->getId()."<br class='spacer'>");

        // test for vulnerability.
        foreach($scan->getPorts() as $port) {
            //echo($port->getCPE()."<br>");

            echo("<br>=== TEST CPE ===<br>");
            echo($port->getCPE() . "<br>");
            echo(" ================<br>");
            $test = explode(':', substr($port->getCPE(),5));

            // we only want to 4 segments type,vendor,product,version
            // don't need to test for that just use those
            $formatted_cpe = "";
            for ($i = 0; $i <4; $i++){
                $matched_segment = [];
                switch($i){
                    case 0:
                        if(preg_match("/^a$/", $test[$i], $matched_segment)){
                            echo("✔️");
                        } else { echo("❌");}
                        break;
                    case 1:
                    case 2:
                        if(preg_match("/^([a-z]|[0-9]|\.|-|_)+$/", $test[$i], $matched_segment)){
                            echo("✔️");
                        } else { echo("❌");}
                        break;
                    case 3:
                        preg_match("/^([0-9]\.){2}[0-9]/", $test[$i], $matched_segment);
                        if($matched_segment[0]){
                            echo("✔️use ".$matched_segment[0]." instead");
                        } else { echo("❌");}
                        break;
                }
                echo($test[$i]. "<br>");
            }
        }



        die;
        return new JsonResponse(["added"]);
    }


    /**
     * @Route("/scan-test", name="app_testing")
     */
    public function scanTest()
    {
        $cpes= [];

        $host = $this->getUser()->getHostByAddress("10.10.10.145");
        $scan = $host->getLastScan();
        //echo($scan->getId()."<br class='spacer'>");

        // test for vulnerability.
        foreach($scan->getPorts() as $port){
            //echo($port->getCPE()."<br>");
            array_push($cpes, substr($port->getCPE(),5));
        }

        //phpinfo();

        // can we connect to mongodb?
        // (doctrine really isn't necessary for this use case (just lookups) and adds unnecessary overhead

        $connection = 'mongodb://localhost:27017';
        $client = new MongoDB\Client($connection);
        // this isn't closed by design https://github.com/mongodb/mongo-php-driver/issues/393
        $db = $client->selectDatabase('cvedb');

        echo($cpes[1]."=== RESULTS === <br>");
        $cursor = $db->cves
            ->find(array("vulnerable_configuration" =>
                array('$regex' => $cpes[1])),
                array('id' => 1, '_id' => 0));

        foreach($cursor as $document){
            echo($document->id." === ".$document->cvss."<br>");
        }

        dd($cursor);



        /*$mongo = new \MongoDB('mongodb://localhost:27017');
        $mongo->connect();
        $collection = $mongo->cvedb->cves;

        dd($collection->find(array()));
            //{"vulnerable_configuration": {"$regex": "a:apache:http_server:2.4.7", "$options" : "i"}},{id : 1, _id : 0});
        */



        die;

        return new JsonResponse(["added"]);
    }
}