<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AccountController extends AbstractController
{
    /**
     * @Route("/account", name="app_account")
     */
    public function index()
    {
        $hosts = $this->getUser()->getHosts();
        $hostCount = count($hosts);

        $pending = [];
        $complete = [];
        $this->getUser()->getCommandCompletionArrays($complete,$pending);

        return $this->render('account/index.html.twig', [
            'hostCount' => $hostCount,
            'scanCount' => '200',
            'pendingCount' => $pending,
            'completeCount' => $complete,
        ]);
    }
}
