<?php

namespace App\Controller;

use App\Entity\ApiToken;
use App\Entity\Command;
use App\Entity\Discovery;
use App\Entity\Host;
use App\Form\CommandType;
use App\Form\DiscoveryType;
use App\Repository\CommandRepository;
use App\Repository\HostRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/host")
 */
class HostController extends AbstractController
{
    /**
     * @Route("/", name="app_host")
     */
    public function index(Request $request, CommandRepository $commandRepository): Response
    {
        // debug testing - adding current pc as a host
        // adding clientHost for display purposes causes hosts in controller and DB to be out of sync
        // requires removal before updating DB

        /*could already exist in DB
        $clientHost = new Host();
        $clientHost->setUser($this->getUser());

        todo - use this to change download guidance pictures,
            if on linux show executing the script with bash/python
            if on Windows show running the script on Windows.
        if (strpos($_SERVER['HTTP_USER_AGENT'], 'Windows')){
            //$clientHost->setDetail("Windows");
        }
        elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'Linux')){
            //$clientHost->setDetail("Linux");
        }
        else{
            // either just say "unknown" or print
            $clientHost->setDetail($_SERVER['HTTP_USER_AGENT']);
        }

        $clientHost->setAddressString($_SERVER['REMOTE_ADDR']);
        $clientHost->setNote("CURRENT_PC");
        $this->getUser()->addHost($clientHost);*/

        $hostList = $this->getUser()->getHosts();
        // get timestamps from command
        //$commands = $commandRepository
        //    ->findBy()

        //$commandList = $this->getUser()->getCommands();

        /*foreach($hostList as $currentHost){
            $discID = $currentHost->getDiscovery();
            $currentHost->setDetail($commandList[$command] WHERE $discID = $id)
        }*/

        // Todo - this page is only to create "discoveries" - unless i add additional misc commands later

        $discovery = new Discovery(
            $this->getUser(),
            "nmap",
            ["n/a"],
            ["10.10.10.1/27"]
        );

        $form = $this->createForm(DiscoveryType::class, $discovery);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            // todo - don't resubmit on refresh

            // we're not showing the hostList anymore + maybe should just put that info in the DB anyway so don't have to remove.
            //$this->getUser()->removeHost($clientHost);

            $entityManager = $this->getDoctrine()->getManager();

            $discovery->setOptions([
                // scan options
                "-sn","-PE",
                // output options, saves in /tmp/ as an xml file -> todo %temp% in windows
                "-oX","/tmp/".$discovery->getOTP()
            ]);
            
            //255.255.255.224 -> /27 -> max 30 hosts (expected 5? online)

            // need to ensure user input matches address

            /* valid internal IP addresses are:
            probably easier to just do this without regex tbh
             10.10.10.1 - 127.0.0.1 - 172.16.0.1 - 192.168.0.1
            $regex_host = ((127\.)|(10\.)|(172\.1[6-9]\.)|(172\.2[0-9]\.)|(172\.3[0-1]\.)|(192\.168\.))
            $regex_127 =
            $regex_10 = (10\.(((1[0-9][0-9]|255|25[0-4]|2([0-4])?[0-9]|[1-9]([0-9])?)\.){2})(1[0-9][0-9]|25[0-4]|2([0-4])?[0-9]|[1-9]([0-9])?))
            $regex_172 = (172\.1[6-9]\.)
            $regex_192 = (192\.168\.)
            $regex0-255 = (1[0-9][0-9]|255|25[0-4]|2([0-4])?[0-9]|[1-9][0-9]?)
            */

            /*foreach($form->get('target')->getData() as $addr){
                //$addr = parse_address($addr);
                dd($addr);
            }*/

            $regex_10 = "(10\.(((1[0-9][0-9]|255|25[0-4]|2([0-4])?[0-9]|[1-9]([0-9])?)\.){2})(1[0-9][0-9]|25[0-4]|2([0-4])?[0-9]|[1-9]([0-9])?))";

            // todo - only supports strings - not arrays yet
            //  debug settings for scan - needs to show confirmation later
            // should be handled by form validation instead?
            if(preg_match($regex_10, $form->get('target')->getData()[0])){
                $discovery->setTarget([$form->get('target')->getData()[0]]);
            }
            else{
                $discovery->setTarget(["10.10.10.1/27"]);
            }

            $entityManager->persist($discovery);
            $entityManager->flush();

            $userOS = "";
            $filename = $this->getUser()->getUsername()."_scanner.py";
            // look for tell-tale string in useragent
            if (strpos($_SERVER['HTTP_USER_AGENT'], 'Windows')){
                $userOS =  "Windows";
            }
            elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'Linux')){
                $userOS = "Linux";
            }

            if($userOS == ""){
                return $this->render('host/success.html.twig', [
                    'filename' => $filename,
                ]);
            }

            return $this->render('host/success.html.twig', [
                'filename' => $filename,
                'userOS' => $userOS,
            ]);

            //return $this->render('host/success.html.twig');
        }

        return $this->render('host/cards.html.twig', [
            //'hosts' => $hostRepository->findAll(),
            'CommandForm' => $form->createView(),
            'hosts' => $hostList,
        ]);
    }

    /**
     * @Route("/{slug}", name="app_host_view")
     */
    public function view(Request $request, HostRepository $hostRepository, $slug): Response
    {
        $host = $this->getUser()->getHostByMd5($slug);
        if(!$host){
            throw $this->createNotFoundException('Host does not exist');
        }

        $command = new Command(
            $this->getUser(),
            "nmap",
            ["xxx"],
            ["yyy"]
        );

        $form = $this->createForm(CommandType::class, $command);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $entityManager = $this->getDoctrine()->getManager();

            // decide by button press, not user input
            $command->setOptions([
                "-sC","-sV",
                // output options, saves in /tmp/ as an xml file -> todo %temp% in windows
                "-oX","/tmp/".$command->getOTP()
            ]);
            $command->setTarget([
                $host->getAddress()
            ]);

            $entityManager->persist($command);
            $entityManager->flush();

            $userOS = "";
            $filename = $this->getUser()->getUsername()."_scanner.py";
            // look for tell-tale string in useragent
            if (strpos($_SERVER['HTTP_USER_AGENT'], 'Windows')){
                $userOS =  "Windows";
            }
            elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'Linux')){
                $userOS = "Linux";
            }

            if($userOS == ""){
                return $this->render('host/success.html.twig', [
                    'filename' => $filename,
                ]);
            }

            return $this->render('host/success.html.twig', [
                'filename' => $filename,
                'userOS' => $userOS,
            ]);
            //return $this->render('host/success.html.twig');
        }

        /* Doing this in API Upload now (for faster page loads)
         * $scan = $host->getLastScan();
        foreach($scan->getPorts() as $port) {
            $cpe = explode(':', substr($port->getCPE(),5));

            // we only want to 4 segments type,vendor,product,version
            // don't need to test for that just use those
            $formatted_cpe = "";
            for ($i = 0; $i <4; $i++){
                $matched_segment = [];
                switch($i){
                    case 0:
                        preg_match("/^a$/", $cpe[$i], $matched_segment);
                        break;
                    case 1:
                    case 2:
                        preg_match("/^([a-z]|[0-9]|\.|-|_)+$/", $cpe[$i], $matched_segment);
                        break;
                    case 3:
                        preg_match("/^([0-9]\.){2}[0-9]/", $cpe[$i], $matched_segment);
                        break;
                }
                // building the regex correct cpe
                //if($matched_segment[0])
                if($formatted_cpe === ""){
                    $formatted_cpe = $matched_segment[0];
                } else {
                    $formatted_cpe = $formatted_cpe.":".$matched_segment[0];
                }
            }
            echo($formatted_cpe. "<br>");
            //$port->setCpe($formatted_cpe);

        }*/

        // are there pending scans
        $pendingBool = false;

        $pending = [];
        $complete = [];
        $this->getUser()->getCommandCompletionArrays($complete,$pending);

        foreach($pending as $currentPending){
            if(in_array($host->getAddress(), $currentPending->getTarget())){
                $pendingBool = true;
                break;
            }
        }


        $scans = $host->getScans();
        if($scans->last()){
        //if(isset($scans)) {

            //get noteworthy services (not application based (DNS,LDAP,etc))
            $scan = $scans->last();
            $notedPorts = [];
            foreach($scan->getPorts() as $port){
                if(!in_array($port->getService(), $notedPorts)){
                    switch($port->getService()){
                        case "domain":
                            array_push($notedPorts, "domain");
                            break;
                        case "ldap":
                            array_push($notedPorts, "ldap");
                            break;
                        case "telnet?":
                            array_push($notedPorts, "telnet");
                            break;
                        case "microsoft-ds":
                            array_push($notedPorts, "smb");
                            break;
                    }
                }
            }
            // does this automatically exclude those that are empty - I think so
            if(isset($notedPorts)){
                return $this->render('host/scan.report.html.twig', [
                    'host' => $host,
                    'selectedScan' => $scan,
                    'pendingScan' => $pendingBool,
                    'extraPorts' => $notedPorts,
                    'CommandForm' => $form->createView(),
                ]);
            }

            // there are some scans, but the scans don't have any "special" ports
            return $this->render('host/scan.report.html.twig', [
                'host' => $host,
                'selectedScan' => $scan,
                'pendingScan' => $pendingBool,

            //'cve' => $host->getCVEs(),
            //'scan' => $host->getScans(),
            'CommandForm' => $form->createView(),
        ]);
        }

        // there are no scans for this host yet
        return $this->render('host/scan.report.html.twig', [
            'host' => $host,
            'pendingScan' => $pendingBool,
            //'cve' => $host->getCVEs(),
            //'scan' => $host->getScans(),
            'CommandForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{slug}/update/note", name="app_host_update_note", methods={"PUT"})
     */
    public function updateHostNote(Request $request, $slug): Response
    {
        $host = $this->getUser()->getHostByMd5($slug);
        if(!$host){
            throw $this->createNotFoundException('Host does not exist');
        }
        // Check new note name is acceptable

        // Set new name
        $host->setNote($request->getContent());
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($host);
        $entityManager->flush();
        return $this->json("update successful", 200);
    }
}
