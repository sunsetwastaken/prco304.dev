<?php

namespace App\Controller;

use App\Entity\CVE;
use App\Entity\Discovery;
use App\Entity\DiscoveryData;
use App\Entity\Host;
use App\Entity\Port;
use App\Entity\Scan;
use App\Repository\CommandRepository;
use App\Repository\HostRepository;
use SimpleXMLElement;
use MongoDB;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;

class ApiController extends AbstractController
{
    /**
     * @Route("/api/command", name="api_command")
     */
    public function index()
    {
        // return the command the user said to execute
        // aiming to make the system modular, "nmap" could later be substituted for something else.
        // the ip section is an array of it's own as it could be single IP addresses or entire networks
        //return $this->json(["nmap","host_discovery",["10.0.0.0/8"]]);
        $commands = $this->getUser()->getCommands();

        // finding earliest non-complete command
        foreach($commands as $command){
            if($command->getCompletionTime() == null){
                return $this->json([
                    // todo: [0] is the first, we want the most recent non complete one in the real thing
                    $command->getModule(),
                    $command->getOptions(),
                    $command->getTarget(),
                    $command->getOTP()
                ]);
            }
        }
        // no incomplete commands
        return new JsonResponse(["No queued commands for this user"]);


        /*return $this->json([
            // todo: [0] is the first, we want the most recent non complete one in the real thing
            $command[0]->getModule(),
            $command[0]->getOptions(),
            $command[0]->getTarget(),
            $command[0]->getOTP()
        ]);
        /* doing a traditional JSON response sends unnecessary data and requires further parsing
        $test = str_replace('\0', "", (array)$command[0]);

        //clear out weird chars
        for($i=0;$i<count((array)$command[0]);$i++){
            $command[0][$i] = trim($command[0][$i]);

                return new JsonResponse($test);
        }*/

        //return new JsonResponse()
        //return new Response(json_encode($this->getUser()));
    }


    /**
     * @Route("/api/upload", name="api_upload")
     */
    public function upload(CommandRepository $commandRepository, Request $request)
    {
        // Todo
        //  differentiate upload types - Host vs Scan results <<< CURRENT

        if(!$request->headers->get('Key')) {
            return new JsonResponse(["No upload key provided"]);
        }
        $command = $commandRepository->findOneBy(['OTP' => $request->headers->get('Key')]);

        if(!$command){
            return new JsonResponse(["Invalid upload key provided"]);
        }
        // $command (upload OTP) vs $this (api key)
        elseif ($command->getUser() != $this->getUser()){
            // trying to upload data in response to scan someone else requested
            return new JsonResponse(["Invalid upload key provided"]);
        }
        // make sure that command isn't already completed
        elseif ($command->getCompletionTime()){
            return new JsonResponse(["Invalid upload key provided"]);
        }

        // dd($key) prints out the command object, could re-implement that in /api/command
        // at this point it's a valid upload, lets check the contents now
        // make sure you don't reupload the same host (based on IP address) - is there a better way? MAC addr?

        $entityManager = $this->getDoctrine()->getManager();
        //dd($request->getContent());
        $xmlUpload = new SimpleXMLElement($request->getContent());

        $detectedOS = "";
        $detectedHostname = "";
        foreach ($xmlUpload as $currentNode){
            if ($currentNode->getName() == "host") {
                // is the uploaded XML referencing an existing host? - if not, create one
                if(!$host = $this->getUser()->getHostByAddress($currentNode->address->attributes()['addr'])) {
                    $host = new Host(
                        $this->getUser(),
                        $currentNode->address->attributes()['addr'],
                        $command,
                        $command
                    );
                }
                else{
                    $host->setLastSeenBy($command);
                }

                // is there a given hostname for this host?

                // DON'T ALWAYS RE-WRITE
                if(isset($currentNode->hostnames->hostname)){
                    //$host->setHostname($currentNode->hostnames->hostname->attributes()['name']);
                    $detectedHostname = $currentNode->hostnames->hostname->attributes()['name'];
                }

                $entityManager->persist($host);

                if(is_a($command, "App\Entity\Discovery")){
                    $data = new DiscoveryData($command, $host);
                    $entityManager->persist($data);
                }

                // are there any ports for this host?
                // Todo - if there are ports make new Scan object
                // is ports always set?
                if(isset($currentNode->ports)){
                    $scan = new Scan($command, $host);

                    //make mongo connection for later CVE lookups (opening now prevents many opening later. especially since there's no close())
                    $connection = 'mongodb://localhost:27017';
                    $client = new MongoDB\Client($connection);
                    // this isn't closed by design https://github.com/mongodb/mongo-php-driver/issues/393
                    $db = $client->selectDatabase('cvedb');

                    foreach($currentNode->ports->port as $currentPort){
                        $port = new Port(
                            $scan,
                            $currentPort->attributes()['portid'],
                            $currentPort->attributes()['protocol'],
                            $currentPort->service->attributes()['name'],
                            $currentPort->service->attributes()['product'],
                            $currentPort->state->attributes()['state'],
                            null
                        );

                        foreach($currentPort->service->cpe as $cpe){
                            // here we check the CPE is for the application, noted by cpe:/a: rather than cpe:/o: for OS
                            // substring has to be used over $cpe[5] here as $cpe is of type simpleXMLelement (which doesn't let you use type conversions either)
                            if(substr($cpe,5,1) == "a"){
                                $valid = true;
                                $formatted_cpe = "a";
                                $nmap_cpe = explode(':', substr($cpe,7));
                                // split into vendor, product, version
                                for ($i = 0; $i <3; $i++){
                                    $matched_segment = [];
                                    if(isset($nmap_cpe[$i])){
                                        //echo("<br>#".$nmap_cpe[$i]);
                                        switch($i){
                                            case 0:
                                            case 1:
                                                // no symbols to be able to inject into MongoDB, no query logic hijacking possible
                                                preg_match("/^([a-z]|[0-9]|\.|-|_)+$/", $nmap_cpe[$i], $matched_segment);
                                                break;
                                            case 2:
                                                //preg_match("/^([0-9]\.){2}[0-9]/", $nmap_cpe[$i], $matched_segment);
                                                //preg_match("/^([0-9]|\.){1,10}/", $nmap_cpe[$i], $matched_segment);

                                                // this regex accepts "correct" 1.2.3 version numbers, and truncates awkward 6.6.1p1 versions to 6.6
                                                // this has been done to tolerate the awkward version listings used in projects like OpenSSH.
                                                preg_match("/^[0-9]{1,6}\.(([0-9]{1,6}\.[0-9]{1,6}$)|([0-9]{1,6}))/", $nmap_cpe[$i],$matched_segment);
                                                break;
                                        }
                                        if(isset($matched_segment[0])){
                                            //echo($matched_segment[0]);//testing why samba on 10.10.10.3 isn't version detected.
                                            // it is detected (to the extent it can be) there just isn't any available info for the application
                                            $formatted_cpe = $formatted_cpe.":".$matched_segment[0];
                                        }else {
                                            $valid = false;
                                            //echo("invalid cpe issue");
                                            //dd($nmap_cpe);
                                        }
                                    }
                                }

                                if($valid){
                                    $port->setCpe($formatted_cpe);

                                    // look up CVE
                                    $cursor = $db->cves
                                        ->find(array("vulnerable_configuration" =>
                                            array('$regex' => $formatted_cpe)),
                                            array('id' => 1, '_id' => 0));

                                    foreach($cursor as $document){
                                        $cve = new CVE(
                                            $port,
                                            $document->id,
                                            $document->cvss,
                                            $document->summary
                                        );
                                        $entityManager->persist($cve);
                                    }
                                }
                            }
                        }
                        $entityManager->persist($port);


                        if($currentPort->service->attributes()['ostype']){
                            $detectedOS = $currentPort->service->attributes()['ostype'];
                        }


                        /*
                            READING THE ELEMENTS THIS DEEP IS BROKEN IN XMLREADER
                               SCRIPTS ARE READ AS ( +"elem": """ with many \n characters at unexpected locations)
                            After debugging I don't think SimpleXMLReader can deal with that,
                            it's pointed at the right location.

                            At this point I don't think this extra accuracy/detection is a good trade off for
                            the time required to replace current XML parsing.


                        echo("BEFORE SCRIPTS");
                        foreach($currentPort->script as $script){
                            dd($script);
                            foreach($script->table as $table){
                                // get commonName from SSL certs *should* 99% be same as hostname, if not it's impersonating it
                                // grab it if we don't have a hostname yet though, it's better than nothing
                                if($table->attributes('key') == "subject" && $detectedHostname == ""){
                                    $detectedHostname = $table->elem;
                                }
                            }
                            // get fqdn (more useful than just hostname) from rdp-ntlm
                            foreach($script->elem as $elem){
                                echo("<br> DO WE FIND -- DNS_COMPUTER_NAME --<br>");
                                if($elem->attributes('key') == "DNS_Computer_Name"){
                                    echo("<br>YES<br>");
                                    $detectedHostname = $elem;
                                }
                                // get fqdn from smb scripts
                                elseif($elem->attributes('key') == "fqdn"){
                                    $detectedHostname = $elem;
                                }
                                elseif ($elem->attributes('key') == "os"){
                                    $detectedOS = $elem;
                                }
                                dd($script);
                            }
                        }
                        */

                        // latest scan is treated as most trusted scan
                        if($detectedOS != ""){
                            $host->setDetail($detectedOS);
                        }
                        if($detectedHostname != ""){
                            $host->setHostname($detectedOS);
                        }
                        $entityManager->persist($host);
                    }
                    $entityManager->persist($scan);
                }
            }
        }
        // update command entity - mark as complete
        $command->setCompletionTime(time());
        //$this->
        $entityManager->persist($command);



        $entityManager->flush();
        return new Response("Successfully added to DB");

    }
}