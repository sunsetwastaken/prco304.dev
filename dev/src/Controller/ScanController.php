<?php

namespace App\Controller;

use App\Entity\Command;
use App\Entity\Discovery;
use App\Form\CommandType;
use App\Form\DiscoveryType;
use App\Form\ScanCommandType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ScanController extends AbstractController
{
    /**
     * @Route("/scan", name="app_scan")
     */
    public function index(Request $request)
    {

        $userOS = "";
        $filename = $this->getUser()->getUsername()."_scanner.py";
        // look for tell-tale string in useragent
        if (strpos($_SERVER['HTTP_USER_AGENT'], 'Windows')){
            $userOS =  "Windows";
        }
        elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'Linux')){
            $userOS = "Linux";
        }

        $command = new Command(
            $this->getUser(),
            "nmap",
            ["xxx"],
            ["yyy"]
        );
        $form = $this->createForm(ScanCommandType::class, $command);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if($command->getModule() == "disc"){
                $discovery = new Discovery(
                    $this->getUser(),
                    "nmap",
                    array(""),
                    array("")
                );
                switch ($command->getOptions()[0]){
                    case 2:
                        $discovery->setOptions(array("-sn","-PE","-T5","-oX","/tmp/".$discovery->getOTP()));
                        break;
                    case 1:
                    default:
                        $discovery->setOptions(array("-sn","-PE","-oX","/tmp/".$discovery->getOTP()));
                        break;
                }
                //check regex again.
                $discovery->setTarget($command->getTarget());

                //push to db
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($discovery);
                $entityManager->flush();

                if($userOS == ""){
                    // don't provide OS-specific instructions
                    return $this->render('host/success.html.twig', [
                        'filename' => $filename,
                    ]);
                }

                return $this->render('host/success.html.twig', [
                    'filename' => $filename,
                    'userOS' => $userOS,
                ]);
            }
            // scan is the only other option currently
            $command->setModule("nmap");

            switch ($command->getOptions()[0]){
                case 3:
                    $command->setOptions(array("-sC","-sV","-p-","-oX","/tmp/".$command->getOTP()));
                    break;
                case 2:
                    $command->setOptions(array("-sC","-sV","-T5","-oX","/tmp/".$command->getOTP()));
                    break;
                case 1:
                default:
                    $command->setOptions(array("-sC","-sV","-oX","/tmp/".$command->getOTP()));
                    break;
            }
            // do regex on target
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($command);
            $entityManager->flush();
            //dd($command);

            if($userOS == ""){
                // don't provide OS-specific instructions
                return $this->render('host/success.html.twig', [
                    'filename' => $filename,
                ]);
            }

            return $this->render('host/success.html.twig', [
                'filename' => $filename,
                'userOS' => $userOS,
            ]);
        }

        $hosts = $this->getUser()->getHosts();

        return $this->render('scan/index.html.twig', [
            'controller_name' => 'ScanController',
            'hosts' => $hosts,
            'CommandForm' => $form->createView()
        ]);
    }
}
