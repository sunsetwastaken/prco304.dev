<?php

/*todo:
    1. !done! Make a token for the user
    2. Only make a token when when isn't already avalible, don't spam on page request
    2. generate the download
    3. present download
    4. display usage imagery etc
*/

namespace App\Controller;

use App\Entity\ApiToken;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\HeaderUtils;

class DownloadController extends AbstractController
{
    /**
     * @Route("/download", name="app_download")
     */
    public function index()
    {
        // ensure their api key is set -> they didn't just come to this page without starting a scan.
        // make this a general function so i dont have to have this code on multiple pages
        foreach($this->getUser()->getApiTokens() as $token){
            if ((int)$token->getExpiry() > (int)time()){
                //token is still valid - renew it for scan
                $currentToken = $token;
                $currentToken->renewExpiry();
                break;
            }
        }
        // if there is no current token make one.
        if(!isset($currentToken)){
            // make them a new token.
            $currentToken = new ApiToken($this->getUser());
        }

        // save renewal or new token
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($currentToken);
        $entityManager->flush();


        // give the user their download file
        // todo - generate and serve file as dl
        //  username may not be suitable filename format
        $filename = $this->getUser()->getUsername()."_scanner.py";
        //$fileContent = "the generated_download goes here.";

        //#!/usr/bin/env python is okay since can be run on 2.x or 3.x
        $scriptBody = "#!/usr/bin/env python
import os
import json
import requests
    
update_url = 'http://polaris.local/api/command'
upload_url = 'http://polaris.local/api/upload'
api_key = '".$currentToken->getToken()."'

def update():
    headers = {
        'Authorization':'Bearer ' + api_key
    }
    r = requests.get(update_url, headers = headers)
    json_data = json.loads(r.text)

    # validation

    module = json_data[0]
    # -oX /tmp/upload_key is a mandatory option
    options = ' '.join(json_data[1])
    target = ' '.join(json_data[2])
    upload_key = json_data[3]
    
    return module,options,target,upload_key

def run_update(module,options,target):
    install_check = os.system(\"which \" + module)
    if(install_check == 0):
        # it exited normally (returning 0)
        run_request = os.system(module + \" \" + options + \" \" + target)

def upload(upload_key):
    headers = {
        'Authorization':'Bearer ' + api_key,
        'key' : upload_key,
        'Content-Type':'text/xml'
    }
    with open('/tmp/' + upload_key, 'rb') as data:
        r = requests.post(
        upload_url,
        headers = headers,
        data = data
        )
        
        #json_data = json.loads(r.text)
        #if(\"success\" in json_data){//all good}



def main():
    module,options,target,upload_key = update()
    run_update(module,options,target)
    upload(upload_key)

if __name__ == '__main__':
    main()";



        $response = new Response($scriptBody);

        // add Content-Disposition header
        $disposition = HeaderUtils::makeDisposition(
            HeaderUtils::DISPOSITION_ATTACHMENT,
            $filename
        );

        $response->headers->set('Content-Disposition', $disposition);
        return $response;
    }
}
