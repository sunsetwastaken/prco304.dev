<?php


namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationFormType;
use App\Form\SmallRegistrationFormType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class RootController extends AbstractController
{
    /**
     * @Route("/")
     */
    public function root(Request $request){

        // debug
        //var_dump($_SERVER);
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);
        // on getting a form submission hand it off to registration controller


        return $this->render('root/index.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }

}