<?php


namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class base64Extension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('base64encode', [$this, 'base64encode']),
            new TwigFilter('base64decode', [$this, 'base64decode']),
        ];
    }

    public function base64encode(string $input)
    {
        return base64_encode($input);
    }
    public function base64decode(string $input)
    {
        return base64_decode($input);
    }

}