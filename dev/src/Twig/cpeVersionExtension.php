<?php


namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class cpeVersionExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('cpeVersion', [$this, 'cpeVersion']),
        ];
    }

    public function cpeVersion(string $input)
    {
        $exploded = explode(':', $input);
        return end($exploded);
    }
}