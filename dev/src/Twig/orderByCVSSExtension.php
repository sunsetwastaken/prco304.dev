<?php


namespace App\Twig;

use Doctrine\ORM\PersistentCollection;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class orderByCVSSExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('orderbyCVSS', [$this, 'orderbyCVSS']),
        ];
    }

    public function cmp($currentCve, $cve){
        return $currentCve->getCvss() <  $cve->getCvss();
    }

    public function orderbyCVSS(PersistentCollection  $cves)
    {
        $orderedCves = $cves->toArray();

        usort($orderedCves, array($this,"cmp"));
        return $orderedCves;
    }
}