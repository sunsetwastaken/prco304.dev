<?php

/* Todo
    0. Adding extra failure notifications - debugging connectivity
    1. Error handling - Invalid keys etc
    3. Error reporting, No Authentication provided is not a fitting response
    2. UX improvements, What if the the user is on a browser
*/

namespace App\Security;

use App\Repository\ApiTokenRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

class ApiAuthenticator extends AbstractGuardAuthenticator
{
    /**
     * @var ApiTokenRepository
     */
    private $apiTokenRepository;

    public function __construct(ApiTokenRepository $apiTokenRepository)
    {
        $this->apiTokenRepository = $apiTokenRepository;
    }

    public function supports(Request $request)
    {
        // should we use this authenticator
        return $request->headers->has('Authorization');
        // return true;

    }

    public function getCredentials(Request $request)
    {
        // get the entire header
        $header = $request->headers->get('Authorization');
        $key = substr($header, 7);
        return $key;

    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        // find the token in the DB
        $key = $this->apiTokenRepository->findOneBy(['token' => $credentials]);
        if($key){
            return $key->getUser();
        }
        return;
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        // todo ensure token is valid - not expired etc && maybe renew it
        return true;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        // to get here you supplied a token, but it wasn't valid
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        // don't need to do anything special
    }

    public function start(Request $request, AuthenticationException $authException = null)
    {
        return new JsonResponse(["Invalid authentication provided"]);
    }

    public function supportsRememberMe()
    {
        // todo
    }
}
