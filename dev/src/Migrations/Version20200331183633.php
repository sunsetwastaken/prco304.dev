<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200331183633 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE discovery (id INT AUTO_INCREMENT NOT NULL, command_id INT NOT NULL, completion_time VARCHAR(180) DEFAULT NULL, UNIQUE INDEX UNIQ_54380C2F33E1689A (command_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE port (id INT AUTO_INCREMENT NOT NULL, scan_id INT NOT NULL, number VARCHAR(10) NOT NULL, protocol VARCHAR(10) NOT NULL, service VARCHAR(180) DEFAULT NULL, version VARCHAR(180) DEFAULT NULL, status VARCHAR(10) NOT NULL, INDEX IDX_43915DCC2827AAD3 (scan_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE scan (id INT AUTO_INCREMENT NOT NULL, command_id INT NOT NULL, host_id INT NOT NULL, completion_time VARCHAR(180) DEFAULT NULL, UNIQUE INDEX UNIQ_C4B3B3AE33E1689A (command_id), INDEX IDX_C4B3B3AE1FB8D185 (host_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE discovery ADD CONSTRAINT FK_54380C2F33E1689A FOREIGN KEY (command_id) REFERENCES command (id)');
        $this->addSql('ALTER TABLE port ADD CONSTRAINT FK_43915DCC2827AAD3 FOREIGN KEY (scan_id) REFERENCES scan (id)');
        $this->addSql('ALTER TABLE scan ADD CONSTRAINT FK_C4B3B3AE33E1689A FOREIGN KEY (command_id) REFERENCES command (id)');
        $this->addSql('ALTER TABLE scan ADD CONSTRAINT FK_C4B3B3AE1FB8D185 FOREIGN KEY (host_id) REFERENCES host (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE port DROP FOREIGN KEY FK_43915DCC2827AAD3');
        $this->addSql('DROP TABLE discovery');
        $this->addSql('DROP TABLE port');
        $this->addSql('DROP TABLE scan');
    }
}
