<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200407165457 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE discovery_data (discovery_id INT NOT NULL, host_id INT NOT NULL, INDEX IDX_D3EA86DD1E7332B (discovery_id), INDEX IDX_D3EA86D1FB8D185 (host_id), PRIMARY KEY(discovery_id, host_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE discovery_data ADD CONSTRAINT FK_D3EA86DD1E7332B FOREIGN KEY (discovery_id) REFERENCES discovery (id)');
        $this->addSql('ALTER TABLE discovery_data ADD CONSTRAINT FK_D3EA86D1FB8D185 FOREIGN KEY (host_id) REFERENCES host (id)');
        $this->addSql('ALTER TABLE host DROP FOREIGN KEY FK_CF2713FDD3EB0CB0');
        $this->addSql('ALTER TABLE host ADD discovery_id INT DEFAULT NULL, DROP discovery');
        $this->addSql('ALTER TABLE host ADD CONSTRAINT FK_CF2713FDD1E7332B FOREIGN KEY (discovery_id) REFERENCES command (id)');
        $this->addSql('ALTER TABLE host ADD CONSTRAINT FK_CF2713FDD3EB0CB0 FOREIGN KEY (last_seen_by_id) REFERENCES command (id)');
        $this->addSql('CREATE INDEX IDX_CF2713FDD1E7332B ON host (discovery_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE discovery_data');
        $this->addSql('ALTER TABLE host DROP FOREIGN KEY FK_CF2713FDD1E7332B');
        $this->addSql('ALTER TABLE host DROP FOREIGN KEY FK_CF2713FDD3EB0CB0');
        $this->addSql('DROP INDEX IDX_CF2713FDD1E7332B ON host');
        $this->addSql('ALTER TABLE host ADD discovery VARCHAR(180) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, DROP discovery_id');
        $this->addSql('ALTER TABLE host ADD CONSTRAINT FK_CF2713FDD3EB0CB0 FOREIGN KEY (last_seen_by_id) REFERENCES discovery (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
    }
}
