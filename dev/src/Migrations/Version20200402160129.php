<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200402160129 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE discovery (id INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE discovery ADD CONSTRAINT FK_54380C2FBF396750 FOREIGN KEY (id) REFERENCES command (id)');
        $this->addSql('ALTER TABLE host ADD last_seen_by_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE host ADD CONSTRAINT FK_CF2713FDD3EB0CB0 FOREIGN KEY (last_seen_by_id) REFERENCES discovery (id)');
        $this->addSql('CREATE INDEX IDX_CF2713FDD3EB0CB0 ON host (last_seen_by_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE host DROP FOREIGN KEY FK_CF2713FDD3EB0CB0');
        $this->addSql('DROP TABLE discovery');
        $this->addSql('DROP INDEX IDX_CF2713FDD3EB0CB0 ON host');
        $this->addSql('ALTER TABLE host DROP last_seen_by_id');
    }
}
