<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200402163442 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        //$this->addSql('ALTER TABLE discovery DROP FOREIGN KEY FK_54380C2FBF396750');
        //$this->addSql('ALTER TABLE discovery ADD module VARCHAR(180) NOT NULL, ADD options JSON DEFAULT NULL, ADD target JSON DEFAULT NULL, ADD creation_time VARCHAR(180) NOT NULL, ADD otp VARCHAR(180) NOT NULL, ADD completion_time VARCHAR(180) DEFAULT NULL, CHANGE id id INT AUTO_INCREMENT NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        //$this->addSql('ALTER TABLE discovery DROP module, DROP options, DROP target, DROP creation_time, DROP otp, DROP completion_time, CHANGE id id INT NOT NULL');
        //$this->addSql('ALTER TABLE discovery ADD CONSTRAINT FK_54380C2FBF396750 FOREIGN KEY (id) REFERENCES command (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
    }
}
