<?php

namespace App\Repository;

use App\Entity\CVE;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method CVE|null find($id, $lockMode = null, $lockVersion = null)
 * @method CVE|null findOneBy(array $criteria, array $orderBy = null)
 * @method CVE[]    findAll()
 * @method CVE[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CVERepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CVE::class);
    }

    // /**
    //  * @return CVE[] Returns an array of CVE objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CVE
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
